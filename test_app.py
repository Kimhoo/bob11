import unittest
import app

class test(unittest.TestCase):
    def setUp(self):
        self.app = app.app.test_client()
        self.test_case = {"a":1,"b":2}
    
    def test_add(self):
        res=self.app.get('/add',query_string=self.test_case)
        res=res.get_data().decode()
        self.assertEqual(str(3),res)
    
    def test_sub(self):
        res=self.app.get('/sub',query_string=self.test_case)
        res=res.get_data().decode()
        self.assertEqual(str(-1),res)
        
if __name__=="__main__":
    unittest.main()