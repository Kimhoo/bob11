from flask import Flask
from flask import request
app = Flask(__name__)


@app.route('/')
def index():
    return 'Hello Bob from user052'   


@app.route('/add',methods=(['GET']))
def add():
    a=request.args.get('a')
    b=request.args.get('b')
    
    return str(int(a)+int(b))

@app.route('/sub',methods=(['GET']))
def sub():
    a=request.args.get('a')
    b=request.args.get('b')
    
    return str(int(a)-int(b))

if __name__=="__main__":
    app.run(host='127.0.0.1', port=8052)  